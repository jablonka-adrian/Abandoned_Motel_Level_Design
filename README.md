# abandoned-hotel
Level design in unreal engine 4

Create a level with gameplay according to following set-up:
● Level is an old abandoned american motel
● Mood concept:

● The player starts in the start point, gets into the building at designated point - the
remaining part of the level is conducted in an indoor area - rooms, corridors etc.
● There are no enemies on this level - the player needs to get to the finish by removing
obstacles and/or resolving simple puzzles.
● The following gameplay elements should be used as a base for the player actions:
○ Open - the player can open doors
○ Push/pull - the player can push/pull some assets, to clear his path.
○ Use - the player can use some assets ( switches, levers, etc.)
○ Pick up - the player can pick up small elements to use them on other assets
(keys, fuses, etc)
○ Smash - the player can destroy some weaker objects by smashing at them
● All above elements should be pointed out to the player by popping up an icon
indicating usability of the particular asset (single key for all interactions)

1

● The following gameplay elements are banned and should not be used:
○ Jumping ( the player cannot jump)
○ Text messages - the player shouldn’t be talking to himself - we assume that
level objective is already known to the player

Technical requirements:

● Gameplay must be created in UE4, using blueprints and UE4 Third Person template.
● Level beat time should be no longer than 10 minutes
● Graphical assets implementation is not considered an advantage - the whole
prototype should be made from basic shapes (these could be boxes)

Points to be evaluated:

● Creativity
● Blueprint usage
● Gameplay flow

Best of luck!